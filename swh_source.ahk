#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force  ; Ensures single instance only.
onexit, exit

currentversion = 2 ; Set this to the version number, this is so the auto updater can detect available updates.
; Please come up with a new way of doing this. Maybe get the build number from BitBucket and compare that?

menu, tray, icon, Shell32.dll, 172, 1 ; 210 no windows, 172 hidden windows

menu, tray, add, About..., about
menu, tray, add, Exit
menu, tray, add
menu, tray, add ; IM EXTRA
menu, tray, add, Windows hidden:, nowindows
menu, tray, disable, Windows hidden:

if not %A_IsCompiled%
	menu, tray, tip, Simple Window Hiding v2.0 Beta`nSOURCE SCRIPT`n`nCtrl+Alt+Click on a window to hide it.`nRight click here to show windows.
else
	menu, tray, tip, Simple Window Hiding v2.0 Beta`n`nCtrl+Alt+Click on a window to hide it.`nIRight click here to show windows.

menu, tray, nostandard

regread, hasrun, HKEY_LOCAL_MACHINE, SOFTWARE\SimpleWindowHidingbeta, hasrun
if hasrun <> 1
	{
	regwrite, REG_SZ, HKEY_LOCAL_MACHINE, SOFTWARE\SimpleWindowHidingbeta, hasrun, 1
	gosub about
	}
urldownloadtofile, http://dl.dropbox.com/u/8607449/simplewindowhidingversion.txt, %A_TEMP%\simplewindowhidingversion.txt
; The above file no longer exists. What a sad story
fileread, latestversion, %A_TEMP%\simplewindowhidingversion.txt
if latestversion > %currentversion%
	{
		msgbox, 4, New Version Availiable, There is a new version of Simple Window Hiding available.`n`nWould you like to go to the website to download it now?
		ifmsgbox yes
			run http://scainburger.weebly.com/productsprograms.html
	}
return




^!lbutton:: 
; Ctrl+Alt+Click
	Loop
	{
		if A_INDEX = 11
		{
			msgbox Only ten windows can be hidden at once.`n`nWant more windows to be hidden?`nRequest it on my site, and I'll update the program...free, of course.
			return
		}
		if windowclass%A_INDEX% =
		{
			detecthiddenwindows on
			mousegetpos,,, winid
			wingettitle, wintitle, ahk_id %winid%
			wingetclass, winclass, ahk_id %winid%
			if winclass in Progman,DV2ControlHost
			{
				msgbox Can't hide desktop or start menu!
				windowclass%A_INDEX% = 
				return
			}
			
			windowclass%A_INDEX% = %winclass%
			windowtitle%A_INDEX% = %wintitle%
			
			winhide, %wintitle% ahk_class %winclass%
			
			menu, tray, add, Show window "%wintitle%", showwin%A_INDEX%
			
			
			break
		}
	}
return

VisitWebsite:
gui, destroy
run http://scainburger.weebly.com/productsprograms.html
return

about:
gui, destroy

gui, font, s16 w700
gui, add, text,,
(
Simple Window Hiding Beta About
)
gui, font, s12 w700
gui, add, text,,
(
By scainburger (Sean Cain)
)
gui, font, s10 w400
gui, add, text,,
(
This is a simple, standalone exe file that hides a window when you hold Ctrl and Alt, then click on it.
The window will then be hidden.
You can show the window again by right-clicking the Simple Window Hiding tray icon and selecting "Show window: (Window Name)"

You can exit the program at any time by clicking on the Simple Window Hiding icon in your system tray (bottom right, next to the clock).
Exiting the program WILL show your hidden windows.

This program was made to be FREEWARE. Not a trial, not a pay-for-updates, 100 percent free!
YOU SHOULD NOT HAVE PAID ANY MONEY AT ALL TO GET THIS PROGRAM!
If you paid for this program, please report who you got it from to me at scainburger@hotmail.com

To make this program start when you start your computer, click the button below.
)

ifexist %a_startup%\SimpleWindowHidingbeta.lnk
	{
		gui, add, text,, Run at startup is currently ON.
		gui, add, button, gdelshortcut, Disable Run At Startup
		gui, add, text,, If the script still will not launch at startup, click the button below.
		gui, add, button, gRunAtStartup, Re-create startup shortcut.
	}
else
	{
		gui, add, text,, Run at startup is currently OFF.
		gui, add, button, gRunAtStartup, Enable Run At Startup
	}

gui, add, text,,
(

This product is free and open source, therefore you may edit and redistribute it.
However, please don't claim this as your own, especially if you have the source script.


Visit my website for the latest version of this program, as well as more programs or to donate. You will also find my email address if you want to contact me or report a bug.
)
gui, add, button, gVisitWebsite, Visit Website
gui, show, Center Autosize, Simple Window Hiding - About
return

runatstartup:
gui, destroy
filecreateshortcut, %a_scriptfullpath%, %A_Startup%\SimpleWindowHidingbeta.lnk
if errorlevel
	msgbox There was an error. Please report this to scainburger@hotmail.com`nError code: create-shortcut_%ERRORLEVEL%
else
	msgbox, Shortcut successfully placed in startup folder.`nThe program will now start with your computer.`n`nRemember, if you move this .exe file, you MUST re-click the "Re-create startup shortcut" button for the program to continue running at startup.
gosub about
return

delshortcut:
gui, destroy
filedelete %A_Startup%\SimpleWindowHidingbeta.lnk
gosub about
return

showwin1:
	winshow %windowtitle1% ahk_class %windowclass1%
	menu, tray, delete, Show window "%windowtitle1%"
	windowclass1 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin2:
	winshow %windowtitle2% ahk_class %windowclass2%
	menu, tray, delete, Show window "%windowtitle2%"
	windowclass2 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin3:
	winshow %windowtitle3% ahk_class %windowclass3%
	menu, tray, delete, Show window "%windowtitle3%"
	windowclass3 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin4:
	winshow %windowtitle4% ahk_class %windowclass4%
	menu, tray, delete, Show window "%windowtitle4%"
	windowclass4 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin5:
	winshow %windowtitle5% ahk_class %windowclass5%
	menu, tray, delete, Show window "%windowtitle5%"
	windowclass5 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin6:
	winshow %windowtitle6% ahk_class %windowclass6%
	menu, tray, delete, Show window "%windowtitle6%"
	windowclass6 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin7:
	winshow %windowtitle7% ahk_class %windowclass7%
	menu, tray, delete, Show window "%windowtitle7%"
	windowclass7 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin8:
	winshow %windowtitle8% ahk_class %windowclass8%
	menu, tray, delete, Show window "%windowtitle8%"
	windowclass8 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin9:
	winshow %windowtitle9% ahk_class %windowclass9%
	menu, tray, delete, Show window "%windowtitle9%"
	windowclass9 = ; set to blank so the first line can recognise there is no window hidden.
	return

showwin10:
	winshow %windowtitle10% ahk_class %windowclass10%
	menu, tray, delete, Show window "%windowtitle10%"
	windowclass10 = ; set to blank so the first line can recognise there is no window hidden.
	return
	
nowindows: ; Although this is not needed for functionality, the script picks up an error if this label doesn't exist.
	return ; So I'll just make it do nothing :)
	
exit:
	winshow %windowtitle1% ahk_class %windowclass1%
	winshow %windowtitle2% ahk_class %windowclass2%
	winshow %windowtitle3% ahk_class %windowclass3%
	winshow %windowtitle4% ahk_class %windowclass4%
	winshow %windowtitle5% ahk_class %windowclass5%
	winshow %windowtitle6% ahk_class %windowclass6%
	winshow %windowtitle7% ahk_class %windowclass7%
	winshow %windowtitle8% ahk_class %windowclass8%
	winshow %windowtitle9% ahk_class %windowclass9%
	winshow %windowtitle10% ahk_class %windowclass10%
	exitapp